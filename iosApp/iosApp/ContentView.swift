import SwiftUI
import shared

struct ContentView: View {
	let greet = Greeting().greet()

    @ObservedObject var viewModel: IosBooksListViewModel
    
    init() {
        self.viewModel = IosBooksListViewModel()
    }
    
	var body: some View {
        VStack {
            if(viewModel.state.isLoading) {
               Text("Loading!!!")
            } else {
                Text("Loaded!!! title = " + viewModel.state.volumes[0].title)
                
                Button(
                    action: { viewModel.onAction(action: BooksListAction.SomeAction())}
                ) {
                    Text("SomeAction")
                }
            }
            
        }
        .onAppear {
            viewModel.subscribe()
        }.onDisappear {
            viewModel.dispose()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
    }
}
