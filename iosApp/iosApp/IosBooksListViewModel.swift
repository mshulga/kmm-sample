//
//  IosBooksListViewModel.swift
//  iosApp
//
//  Created by Максим Шульга on 27.07.2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import shared

extension ContentView {
    @MainActor class IosBooksListViewModel: ObservableObject {
        private let viewModel: BooksListViewModel
        
        @Published var state: BooksListState = BooksListState(isLoading: true, volumes: [])
        
        private var handle: Closeable?
        
        init() {
            self.viewModel = BooksListViewModel()
        }
        
        func onAction(action: BooksListAction) {
            viewModel.onAction(action: action)
        }
        
        func subscribe() {
            handle = viewModel.uiState.subscribe(onEach: { state in
                if let state = state {
                    self.state = state
                }
            })
        }
        
        func dispose() {
            handle?.close()
        }
    }
}
