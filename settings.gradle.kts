dependencyResolutionManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

rootProject.name = "kmm-sample"
include(":androidApp")
include(":shared")

includeBuild("build-logic")
