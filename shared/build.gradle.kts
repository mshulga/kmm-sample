plugins {
    id("multiplatform-library-convention")
    kotlin("native.cocoapods")
    kotlin("plugin.serialization")
    id("dev.icerock.moko.kswift")
}

kotlin {
    cocoapods {
        summary = "Some description for the Shared Module"
        homepage = "Link to the Shared Module homepage"
        version = "1.0"
        ios.deploymentTarget = "14.1"
        podfile = project.file("../iosApp/Podfile")
        framework {
            baseName = "shared"

            export(libs.moko.mvvw.core)
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(libs.bundles.ktor.multiplatform)
                implementation(libs.kotlinx.coroutines.core)
                implementation(libs.kotlinx.serialization.json)
                implementation(libs.napier)
                api(libs.moko.mvvw.core)
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(libs.ktor.client.okhttp)
            }
        }

        val iosSimulatorArm64Main by getting

        val iosMain by getting {
            dependencies {
                implementation(libs.ktor.client.ios)
            }
            iosSimulatorArm64Main.dependsOn(this)
        }
    }
}

android {
    namespace = "com.mshulga.kmmSample"
}

kswift {
    install(dev.icerock.moko.kswift.plugin.feature.SealedToSwiftEnumFeature)
}