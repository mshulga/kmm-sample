package com.mshulga.kmmSample.network

import io.ktor.client.engine.*
import io.ktor.client.engine.okhttp.*
import java.util.concurrent.TimeUnit

actual fun createHttpClientEngine(): HttpClientEngine =
    OkHttp.create {
        config {
            config {
                retryOnConnectionFailure(true)
                connectTimeout(5, TimeUnit.SECONDS)
            }
        }
    }