package com.mshulga.kmmSample

class AndroidPlatform : Platform {
    override val name: String = "Android ${android.os.Build.VERSION.SDK_INT}"
}

actual fun getPlatform(): Platform = AndroidPlatform()

internal actual val DEFAULT_VOLUMES_QUERY: String = "Android"