package com.mshulga.kmmSample.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

actual class CommonFlow<T> actual constructor(origin: Flow<T>) : Flow<T> by origin
actual open class CommonMutableStateFlow<T> actual constructor(origin: MutableStateFlow<T>) :
    MutableStateFlow<T> by origin

actual open class CommonStateFlow<T> actual constructor(origin: StateFlow<T>) : StateFlow<T> by origin