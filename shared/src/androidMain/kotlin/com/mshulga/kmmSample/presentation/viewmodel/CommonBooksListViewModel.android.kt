package com.mshulga.kmmSample.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mshulga.kmmSample.domain.BooksRepository
import com.mshulga.kmmSample.flow.CommonStateFlow
import com.mshulga.kmmSample.presentation.model.BooksListAction
import com.mshulga.kmmSample.presentation.model.BooksListState

actual class BooksListViewModel : ViewModel() {

    private val commonViewModel: CommonBooksListViewModel by lazy {
        CommonBooksListViewModel(BooksRepository.create(), viewModelScope)
    }

    actual val uiState: CommonStateFlow<BooksListState> = commonViewModel.uiState

    actual fun onAction(action: BooksListAction) {
        commonViewModel.onAction(action)
    }
}