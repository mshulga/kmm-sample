package com.mshulga.kmmSample.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun interface Closeable {
    fun close()
}

actual open class CommonFlow<T> actual constructor(
    private val origin: Flow<T>
) : Flow<T> by origin {

    fun subscribe(
        onEach: (T) -> Unit
    ): Closeable {
        val job = Job()
        onEach {
            onEach(it)
        }.launchIn(CoroutineScope(Dispatchers.Main + job))

        return Closeable { job.cancel() }
    }
}

actual open class CommonMutableStateFlow<T> actual constructor(
    private val origin: MutableStateFlow<T>
) : MutableStateFlow<T>, CommonStateFlow<T>(origin) {
    override val replayCache: List<T>
        get() = origin.replayCache

    override val subscriptionCount: StateFlow<Int>
        get() = origin.subscriptionCount

    override var value: T
        get() = super.value
        set(value) {
            origin.value = value
        }

    override fun compareAndSet(expect: T, update: T): Boolean {
        return origin.compareAndSet(expect, update)
    }

    @ExperimentalCoroutinesApi
    override fun resetReplayCache() {
        origin.resetReplayCache()
    }

    override fun tryEmit(value: T): Boolean {
        return origin.tryEmit(value)
    }

    override suspend fun emit(value: T) {
        origin.emit(value)
    }

    override suspend fun collect(collector: FlowCollector<T>): Nothing {
        origin.collect(collector)
    }
}

actual open class CommonStateFlow<T> actual constructor(
    private val origin: StateFlow<T>
) : StateFlow<T>, CommonFlow<T>(origin) {
    override val replayCache: List<T>
        get() = origin.replayCache

    override val value: T
        get() = origin.value

    override suspend fun collect(collector: FlowCollector<T>): Nothing {
        origin.collect(collector)
    }
}