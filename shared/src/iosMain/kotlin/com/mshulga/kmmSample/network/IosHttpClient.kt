package com.mshulga.kmmSample.network

import io.ktor.client.engine.*
import io.ktor.client.engine.darwin.*

actual fun createHttpClientEngine(): HttpClientEngine =
    Darwin.create {
        configureRequest {
            setAllowsCellularAccess(true)
        }
    }