package com.mshulga.kmmSample.presentation.viewmodel

import com.mshulga.kmmSample.domain.BooksRepository
import com.mshulga.kmmSample.flow.CommonStateFlow
import com.mshulga.kmmSample.presentation.model.BooksListAction
import com.mshulga.kmmSample.presentation.model.BooksListState

actual class BooksListViewModel {

    private val commonViewModel: CommonBooksListViewModel by lazy {
        CommonBooksListViewModel(BooksRepository.create())
    }

    actual val uiState: CommonStateFlow<BooksListState> = commonViewModel.uiState

    actual fun onAction(action: BooksListAction) {
        commonViewModel.onAction(action)
    }
}