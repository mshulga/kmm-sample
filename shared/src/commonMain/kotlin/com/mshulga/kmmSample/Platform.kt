package com.mshulga.kmmSample

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform

internal expect val DEFAULT_VOLUMES_QUERY: String