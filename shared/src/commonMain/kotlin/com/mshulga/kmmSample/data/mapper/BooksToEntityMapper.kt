package com.mshulga.kmmSample.data.mapper

import com.mshulga.kmmSample.data.model.VolumeDto
import com.mshulga.kmmSample.data.model.VolumeDtoWrapper
import com.mshulga.kmmSample.domain.model.VolumeEntity

internal class BooksToEntityMapper {

    fun mapVolumeWrapper(dto: VolumeDtoWrapper): List<VolumeEntity> =
        dto.items.map(::mapVolume)

    private fun mapVolume(dto: VolumeDto): VolumeEntity =
        with(dto) {
            VolumeEntity(
                id = id,
                title = volumeInfo.title,
                authors = volumeInfo.authors,
                description = volumeInfo.description,
                thumbnail = volumeInfo.imageLinks.thumbnail
            )
        }
}