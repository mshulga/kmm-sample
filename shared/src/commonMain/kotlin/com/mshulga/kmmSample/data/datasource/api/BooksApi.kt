package com.mshulga.kmmSample.data.datasource.api

import com.mshulga.kmmSample.data.model.VolumeDtoWrapper

internal interface BooksApi {
    suspend fun getVolumes(query: String): VolumeDtoWrapper
}