package com.mshulga.kmmSample.data.repository

import com.mshulga.kmmSample.data.datasource.api.BooksApi
import com.mshulga.kmmSample.data.mapper.BooksToEntityMapper
import com.mshulga.kmmSample.domain.BooksRepository
import com.mshulga.kmmSample.domain.model.VolumeEntity

internal class BooksRepositoryImpl (
    private val api: BooksApi,
    private val toEntityMapper: BooksToEntityMapper,
) : BooksRepository {
    override suspend fun getVolumes(query: String): List<VolumeEntity> {
        val dto = api.getVolumes(query = query)
        return toEntityMapper.mapVolumeWrapper(dto)
    }

}