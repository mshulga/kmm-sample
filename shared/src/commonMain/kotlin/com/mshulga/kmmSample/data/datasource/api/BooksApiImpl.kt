package com.mshulga.kmmSample.data.datasource.api

import com.mshulga.kmmSample.data.model.VolumeDtoWrapper
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

internal class BooksApiImpl(private val httpClient: HttpClient) : BooksApi {
    override suspend fun getVolumes(query: String): VolumeDtoWrapper =
        httpClient.get("/books/v1/volumes") {
            url {
                parameters.append("q", query)
            }
        }.body()
}