package com.mshulga.kmmSample.data.model

import kotlinx.serialization.Serializable

@Serializable
internal data class VolumeDtoWrapper(
    val totalItems: Int,
    val items: List<VolumeDto>
)

@Serializable
internal data class VolumeDto(
    val id: String,
    val volumeInfo: VolumeInfoDto
)

@Serializable
internal data class VolumeInfoDto(
    val title: String,
    val authors: List<String> = arrayListOf(),
    val description: String,
    val imageLinks: VolumeInfoImage
)

@Serializable
internal data class VolumeInfoImage(val thumbnail: String)