package com.mshulga.kmmSample.network

import io.github.aakira.napier.Napier
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

expect fun createHttpClientEngine(): HttpClientEngine

internal fun httpClient(withLog: Boolean) = HttpClient(createHttpClientEngine()) {
    if (withLog) install(Logging) {
        level = LogLevel.ALL
        logger = object : Logger {
            override fun log(message: String) {
                Napier.v(tag = "AndroidHttpClient", message = message)
            }
        }
    }
    install(ContentNegotiation) {
        json(
            json = Json {
                ignoreUnknownKeys = true
            }
        )
    }

    defaultRequest {
        url {
            protocol = URLProtocol.HTTPS
            host = "www.googleapis.com"
            parameters.append("key", "AIzaSyArqSnozA9aCi5Hud_qKXLkpUILq0vC9tQ")
        }

    }
}