package com.mshulga.kmmSample.presentation.viewmodel

import com.mshulga.kmmSample.DEFAULT_VOLUMES_QUERY
import com.mshulga.kmmSample.domain.BooksRepository
import com.mshulga.kmmSample.flow.CommonStateFlow
import com.mshulga.kmmSample.presentation.model.BooksListAction
import com.mshulga.kmmSample.presentation.model.BooksListAction.OnBookClickAction
import com.mshulga.kmmSample.presentation.model.BooksListAction.SomeAction
import com.mshulga.kmmSample.presentation.model.BooksListReducer.loaded
import com.mshulga.kmmSample.presentation.model.BooksListState
import com.mshulga.kmmSample.presentation.redux.Store
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

expect class BooksListViewModel {
    val uiState: CommonStateFlow<BooksListState>
    fun onAction(action: BooksListAction)
}

internal class CommonBooksListViewModel(
    private val repository: BooksRepository,
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.Main),
) : Store<BooksListState, BooksListAction>() {

    init {
        loadVolumes()
    }

    private fun loadVolumes() {
        scope.launch {
            val volumes = repository.getVolumes(DEFAULT_VOLUMES_QUERY)
            updateState { loaded(volumes) }
        }
    }

    override fun getInitialState(): BooksListState = BooksListState.initialState

    override fun onAction(action: BooksListAction) = when (action) {
        is OnBookClickAction -> Napier.e("OnBookClickActionOccurred ${action.bookId}")
        SomeAction -> Napier.e("SomeActionOccurred")
    }
}