package com.mshulga.kmmSample.presentation.model

import com.mshulga.kmmSample.domain.model.VolumeEntity
import com.mshulga.kmmSample.presentation.redux.Action
import com.mshulga.kmmSample.presentation.redux.State

data class BooksListState(
    val isLoading: Boolean,
    val volumes: List<VolumeEntity>,
) : State {
    companion object {
        val initialState = BooksListState(
            isLoading = true,
            volumes = arrayListOf(),
        )
    }
}

sealed class BooksListAction : Action {
    object SomeAction : BooksListAction()
    data class OnBookClickAction(val bookId: String) : BooksListAction()
}

object BooksListReducer {
    fun BooksListState.loaded(volumes: List<VolumeEntity>) = copy(
        volumes = volumes,
        isLoading = false,
    )
}