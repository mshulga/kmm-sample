package com.mshulga.kmmSample.presentation.redux

import com.mshulga.kmmSample.flow.CommonStateFlow
import com.mshulga.kmmSample.flow.toCommonStateFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

interface State
interface Action

abstract class Store<S : State, A : Action> {
    private val mutableUiState: MutableStateFlow<S> by lazy { MutableStateFlow(getInitialState()) }
    val uiState: CommonStateFlow<S> by lazy { mutableUiState.toCommonStateFlow() }

    protected abstract fun getInitialState(): S

    abstract fun onAction(action: A)

    protected fun updateState(modifier: S.() -> S) {
        mutableUiState.update { it.modifier() }
    }
}