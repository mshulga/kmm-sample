package com.mshulga.kmmSample.domain

import com.mshulga.kmmSample.data.datasource.api.BooksApiImpl
import com.mshulga.kmmSample.data.mapper.BooksToEntityMapper
import com.mshulga.kmmSample.data.repository.BooksRepositoryImpl
import com.mshulga.kmmSample.domain.model.VolumeEntity
import com.mshulga.kmmSample.network.httpClient

internal interface BooksRepository {
    suspend fun getVolumes(query: String): List<VolumeEntity>

    companion object {
        fun create(): BooksRepository {
            return BooksRepositoryImpl(
                api = BooksApiImpl(httpClient(true)),
                toEntityMapper = BooksToEntityMapper()
            )
        }
    }
}