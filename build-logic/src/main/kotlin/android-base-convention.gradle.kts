import com.android.build.gradle.BaseExtension

configure<BaseExtension> {
    compileSdkVersion((findProperty("android.compileSdk") as String).toInt())

    defaultConfig {
        minSdk = (findProperty("android.minSdk") as String).toInt()
        targetSdk = (findProperty("android.targetSdk") as String).toInt()
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}