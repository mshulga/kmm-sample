buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }

    dependencies {
        classpath(libs.bundles.plugins)
        classpath(":build-logic")
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
