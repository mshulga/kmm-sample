package com.mshulga.kmmSample.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.mshulga.kmmSample.presentation.model.BooksListAction.OnBookClickAction
import com.mshulga.kmmSample.presentation.model.BooksListAction.SomeAction
import com.mshulga.kmmSample.presentation.viewmodel.BooksListViewModel

class MainActivity : ComponentActivity() {

    private val viewModel: BooksListViewModel by viewModels<BooksListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                val state by viewModel.uiState.collectAsState()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    if (state.isLoading) {
                        CircularProgressIndicator()
                    } else {
                        Column {
                            Button(onClick = { viewModel.onAction(SomeAction) }) {
                                Text(text = "OnSomeActionClick")
                            }
                            Button(onClick = { viewModel.onAction(OnBookClickAction("BookId")) }) {
                                Text(text = "OnBookClickAction")
                            }
                        }
                    }

                }
            }
        }
    }
}

@Composable
fun GreetingView(text: String) {
    Text(text = text)
}

@Preview
@Composable
fun DefaultPreview() {
    MyApplicationTheme {
        GreetingView("Hello, Android!")
    }
}
